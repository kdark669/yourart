<?php 
/*
Plugin Name: Your Artsssss
Plugin URI: https://itglance.net/plugin/itglance/
Description: ITGlance Portfolio
Author: ITGlance
Version: 1.0
Author URI: https://itglance.net/
Text Domain: itg-portfolio
*/



if ( !defined( 'ABSPATH' ) ) exit();

if (!defined('ITGYA_VERSION')) {
    define('ITGYA_VERSION', '1.0.1'); // plugin version
}
if (!defined('ITGYA_PLUGIN_DIR')) {
    define('ITGYA_PLUGIN_DIR', dirname(__FILE__)); // plugin dir
}
if (!defined('ITGYA_ADMIN_DIR')) {
    define('ITGYA_ADMIN_DIR', ITGYA_PLUGIN_DIR . '/includes/admin'); // plugin admin dir
}
if (!defined('ITGYA_PUBLIC_DIR')) {
    define('ITGYA_PUBLIC_DIR', ITGYA_PLUGIN_DIR . '/includes/public'); // plugin admin dir
}
if (!defined('ITGYA_PLUGIN_URL')) {
    define('ITGYA_PLUGIN_URL', plugin_dir_url(__FILE__)); // plugin url
}

if (!class_exists('YourArts')) {
    class YourArts {

        static $_instance = null;

        function __construct()
        {
            $this->setupDatabase();
            $this->initializeHooks();
            $this->createRoles();
            $this->createCustomPages();
            $this->createAdminMenu();
            $this->createArtMenuWoocommerce();
        }

        private function initializeHooks()
        {
            if (is_admin()) {
                require_once('includes/admin/admin.php');
            }
            require_once('includes/public/public.php');
        }

        public static function instance()
        {
            if (!empty(self::$_instance)) {
                return self::$_instance;
            }
            return self::$_instance = new self();
        }

        private function setupDatabase()
        {
            require_once('includes/Database/db.php');
            register_activation_hook(__FILE__,    array('DB', 'createDatabase'));
            register_deactivation_hook(__FILE__,  array('DB', 'removeDatabase')); 
        }
        private function createRoles()
        {
            require_once('includes/role.php');
            register_activation_hook(__FILE__,    array('Role', 'createRole'));
        }
        private function createCustomPages()
        {
            require_once('includes/public/class_ya_custom_page.php');
            register_activation_hook(__FILE__,    array('YA_Custom_Page', 'ya_create_custom_page'));
            add_filter('page_template', array('YA_Custom_Page', 'ya_catch_plugin_template'));
        }

        public function createAdminMenu()
        {
            //creating art menu in admin : art cpt
            if (is_admin()) {
                require_once(ITGYA_PLUGIN_DIR . '/includes/admin/class_ya_admin_menu.php');
                add_action('init', array('Ya_Admin_Menu', 'create_custom_admin_menu'));

                //for adding custom columns
                add_action('manage_art_posts_custom_column',      array('Ya_Admin_Menu', 'custom_art_column'), 10, 2);
                //add data to custom columns
                add_filter('manage_art_posts_columns',             array('Ya_Admin_Menu', 'set_custom_art_column'));
            }
            
        }

        //for menu at woocommerce dashboard
        public function createArtMenuWoocommerce(){
                require_once(ITGYA_PLUGIN_DIR . '/includes/admin/class_ya_art_woo.php');

                //add art menu in woocommece dashboard menu;
                add_action('init',                                 array('Ya_Art_Woo_Menu', 'my_account_add_art'));
                //end point of art add 
                add_action('woocommerce_account_artist_endpoint',  array('Ya_Art_Woo_Menu', 'art_endpoint_content'));
                add_filter('woocommerce_account_menu_items',      array('Ya_Art_Woo_Menu', 'my_account_art_menu_order'));
            
        }

        
        
    }
    function ITGYA()
    {
        return YourArts::instance();
    }

    $GLOBALS['ITGYA'] = ITGYA();
}
