<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('Ya_Art_Woo_Menu')) :
    class Ya_Art_Woo_Menu
    {
        //add the art menu 
        public static function my_account_add_art()
        {
            add_rewrite_endpoint('artist', EP_ROOT | EP_PAGES);
        }

        public static function art_endpoint_content()
        {
            $user = wp_get_current_user();
            $user_roles = $user->roles[0];
            $user_id = $user->ID;
            $url = site_url() . '?author=' . $user_id;

            if ($user_roles == 'artist') {
                wp_redirect($url);
                } else {
                ?>
                <div class="onlyAdmin">
                    <p>Would You Like To Be An Artist</p>
                    <form action="" method="post">
                        <button type='submit' name="beArtist" class="btn"> Yes</button>
                        <?php
                        if (isset($_POST['beArtist'])) {
                            $u = new WP_User($user->ID);
                            $u->remove_role('customer');
                            $u->add_role('artist');
                            header('Location: ' . $_SERVER['REQUEST_URI']);
                        }
                        ?>
                    </form>
                </div>
            <?php
            }
        }
        public static function my_account_art_menu_order()
        {
            $menuOrder = array(
                'dashboard'          => __('Dashboard', 'woocommerce'),
                'artist'             => __('Artist', 'woocommerce'),
                'orders'             => __('Your Orders', 'woocommerce'),
                'downloads'          => __('Download', 'woocommerce'),
                'edit-address'       => __('Addresses', 'woocommerce'),
                'edit-account'        => __('Account Details', 'woocommerce'),
                'customer-logout'    => __('Logout', 'woocommerce'),

            );
            return $menuOrder;
        }
    }

endif;
return new Ya_Art_Woo_Menu();
