<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('Ya_Admin_Menu')) :
    class Ya_Admin_Menu
    {
        //register cpt of art 
        public static function create_custom_admin_menu()
        {
            $labels = [
                'name'               => 'Arts',
                'singular'           => 'Arts',
                'add_new'            => 'Add Art',
                'all_items'          => 'All Arts',
                'add_new_item'       => 'Add Art',
                'edit_item'          => 'Edit Art',
                'new_item'           => 'New Art',
                'view_item'          => 'View Art',
                'search_item'        => 'Search Art  ',
                'not_found'          =>  'No Art found',
                'not_found_in_trash' =>  'No Art found in the Trash',
                'parent_item_colon'  =>  'Parent Item'
            ];
            $args  = [
                'labels' => $labels,
                'public' => true,
                'has_archive' => false,
                'publicly_queryable' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'art',
                    'with_front' => true,
                ),
                'capability_type' => 'post',
                'menu_position' => 5,
                'menu_icon' => 'dashicons-art',
                'supports' => ['title', 'description', 'editor', 'thumbnail', 'comments', 'revisions'],
                'exclude_from_search' => false
            ];
            register_post_type('art', $args);
        }

        //add custom table
        public static function set_custom_art_column($columns)
        {
            unset($columns['author']);
            unset($columns['categories']);
            $columns['Associated'] = __('Associated Product', 'Associated');
            $columns['art_action'] = __('Action', 'Action');
            $columns['featured_image'] = __('Art Image', 'Image');
            return $columns;
        }

        //setting data to custom columns
        public static function custom_art_column($column, $post_id)
        {
            switch ($column) {
                case 'art_action':
                    $terms = get_the_term_list($post_id, 'Action', '', ',', '');
                    if (is_string($terms)) {
                        echo 'Accepted';
                    } else {
                        $postmeta = get_post_meta($post_id);
                        $acceptedFlag = $postmeta['_acceptedFlag'][0];
                        if ($acceptedFlag == 1) {
                            echo '<span id="bm_approved" class="bm_approved">Accepted</span>';
                        } else {
                            wp_enqueue_style('bmapprove', ITGYA_PLUGIN_URL . '/includes/admin/assets/css/bmapprove.css');
                            wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
                            // wp_enqueue_script('bmapprove', ITGYA_PLUGIN_URL . '/includes/admin/assets/js/approve.js', array(), false, true);
?>
                            <input type="hidden" id="post_id" name="post_id" value="<?php echo $post_id ?>">
                            <button type="submit" id="bm-approved-button" class="bm-approved-button" style="cursor: pointer;" onclick="approve(event)">Accept</button>
                            <script>
                                function approve(event) {
                                    event.preventDefault();
                                    const post_id = document.getElementById('post_id').value;
                                    let url = window.location.href + '';
                                    let data = {};
                                    data = new FormData();
                                    data.append('post_id', post_id);
                                    axios.post(url, data, {
                                        params: {
                                            'custom': 'approveArt'
                                        }
                                    }).then(res => {
                                        if (res.data.status) {
                                            // location.reload();
                                            var bmacceptbutton = document.getElementsById('bm-approved-button');
                                            var bmapproved = document.getElementsById('bm_approved');
                                            bmacceptbutton.style.display = 'none';
                                            bmapproved.style.display = 'flex';
                                            bmapproved.innerHTML = "Approved";
                                        }
                                    }).catch(error => {

                                    })
                                }
                            </script>
                            <!-- <form action="" method="post" class="bmapprove" id="bmapprove">
                                <input type="hidden" id="post_id" name="post_id" value="<?php echo $post_id ?>">
                                <button type="submit" id="bm-approved-button" class="bm-approved-button" style="cursor: pointer;">Accept</button>
                            </form> -->
                            <span id="bm_approved" class="bm_approved"></span>
                    <?php

                        }
                    }
                    break;

                case 'featured_image':
                    $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
                    ?>
                    <img src='<?php echo $featured_image[0]; ?>' alt='Photo' class='thumbphoto' style="height:150px; width:150px;">
<?php
                    break;
                case 'Associated':
                    global $wpdb;
                    global $post;
                    $postProductTable = $wpdb->prefix . 'itg_your_art';
                    $sql = "SELECT product_id
                            FROM $postProductTable
                            WHERE post_id = $post_id";
                    $artist = $wpdb->get_results($sql);
                    foreach ($artist as $a) {
                        $postmeta = wp_get_post_categories($a);
                        $product = wc_get_product($a->product_id)->get_title();
                        echo '<span>' . $product . '</span> <br>';
                    }
                    break;
            }
        }
    }

endif;
return new YA_Custom_Page();
