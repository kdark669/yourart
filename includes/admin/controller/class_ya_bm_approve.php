<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('Ya_Art_Approve')) :
    class Ya_Art_Approve
    {
        public function __construct()
        {
            add_action('admin_init', array($this, 'handle'));
            add_action('init', array($this, 'handle'));
        }

        public function handle()
        {
            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) && $_GET['custom'] == "approveArt") {
                global $wpdb;
                $arr = get_cat_ID('uncategorized');
                $termTable = $wpdb->prefix . 'terms';
                $artistTable = $wpdb->prefix . 'art_itg';
                $post_id = sanitize_text_field($_POST['post_id']);
                $categories_uncategorized = $wpdb->get_results("SELECT term_id FROM $termTable where slug LIKE 'uncategorized'");
                $this->cat_array = array_map(function ($value) {
                    return $value->term_id;
                }, $categories_uncategorized);
                $post = get_post($post_id);
                $table = $wpdb->prefix . 'itg_your_art';
                $products = $wpdb->get_results(" SELECT product_id FROM $table where post_id = $post_id");
                foreach ($products as $prod) {
                    
                    $terms = get_the_terms($prod->product_id, 'product_cat');
                    $productToBeCloned = wc_get_product($prod->product_id);
                    $productToBeClonedDescription = get_post($prod->product_id)->post_content;
                    $post_categories = wp_get_post_categories($prod->product_id);
                    $categoryToSet = array_filter($terms, function ($value) {
                        return !in_array($value->term_id, $this->cat_array);
                    });
                    $catToSet = array_map(function ($value) {
                        return $value->term_id;
                    }, $categoryToSet);

                    $product = array(
                        'post_author' => $post->post_author,
                        'post_title' => $post->post_title . ' - ' . $productToBeCloned->get_title(),
                        'post_content' => $productToBeClonedDescription,
                        'post_status' => 'publish',
                        'post_type' => 'product',
                    );
                    $product_id = wp_insert_post($product, '');
                    $products = wc_get_product($product_id);
                    $products->set_category_ids($catToSet);
                    $products->save();
                    $data = get_post_custom($prod->product_id);
                    foreach ($data as $key => $values) {
                        foreach ($values as $value) {
                            add_post_meta($product_id, $key, $value);
                            update_post_meta($product_id, '_postType', $post_id);
                            update_post_meta($product_id, '_selectartist', $post->post_author);
                            update_post_meta($post_id, '_acceptedFlag', 1);
                        }
                    }
                    $media = get_post_thumbnail_id($post_id);
                    update_post_meta($product_id, '_product_image_gallery', $media . ',');
                }
                echo json_encode(array('status' => true));
                wp_redirect(admin_url('/edit.php?post_type=art'));
                die();
                
            }
        }
    }

    new Ya_Art_Approve();
endif;
