<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('Role')) :
    class Role
    {
        public static function createRole()
        {
            add_role(
                'artist',
                'Artist',
                array(
                    'read'         => true,
                    'delete_posts' => false
                )
            );
        }
    }
endif;
