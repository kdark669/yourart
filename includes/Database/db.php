<?php
if (!defined('ABSPATH')) {
    die();
}
if (!class_exists('DB')) {
    class DB{
        public function getTableName()
        {
            global $wpdb;
            return $wpdb->prefix . 'itg_your_art';
        }
        public function getTableNameSecond()
        {
            global $wpdb;
            return $wpdb->prefix . 'itg_art_profile';
        }

        public function createDatabase(){
            global $wpdb;
            $user_table = $wpdb->prefix . 'users';
            $post_table = $wpdb->prefix .'posts';
            $table_name =self::getTableName();
            $table_name_second =self::getTableNameSecond();
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name(
				id int(11) NOT NULL AUTO_INCREMENT,
				product_id bigint(11) unsigned NOT NULL,
				post_id bigint(11) unsigned NOT NULL,
				PRIMARY KEY  (id),
				FOREIGN KEY (product_id) REFERENCES $post_table(`ID`)
				) $charset_collate";

            $sql2 = "CREATE TABLE $table_name_second(
				id int(11) NOT NULL AUTO_INCREMENT,
				user_id bigint(11) unsigned NOT NULL UNIQUE,
				profile varchar(100) NOT NULL,
                created datetime NOT NULL,
				PRIMARY KEY  (id),
				FOREIGN KEY (user_id) REFERENCES $user_table(`id`)
				) $charset_collate";
            $wpdb->query($sql2);
            $wpdb->query($sql);        
        }

        public function removeDatabase(){
            global $wpdb;
            $table_name  = self::getTableName();
            $table_name_second = self::getTableNameSecond();
            $sql = "DROP TABLE IF EXISTS $table_name";
            $sql2 = "DROP TABLE IF EXISTS $table_name_second";
            $wpdb->query($sql2);
            $wpdb->query($sql);
        }
    }
}
?>