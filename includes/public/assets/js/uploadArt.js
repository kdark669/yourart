var activeFlag = true;
function validateImageTile(e) {
    var art_image = document.getElementById('fileElem').value;
    var eart_image = document.getElementById('eart_image');
    if (!art_image) {
        eartist_file.innerHTML = 'Images is Required';
        eartist_file.style.display = 'none';
        activeFlag = false;
    }
    else {
        var eart_image = document.getElementById('eart_image');
        eart_image.style.display = 'none';
        activeFlag = true;
    }
}
function validateArtTitle() {
    var art_title = document.getElementById('art_title').value;
    if (!art_title) {
        var eart_title = document.getElementById('eart_title');
        eart_title.innerHTML = 'Title is Required';
        eart_title.style.display = 'block';
        activeFlag = false;
    } else {
        var eart_title = document.getElementById('eart_title');
        if (eart_title) {
            eart_title.style.display = 'none';
            activeFlag = true;
        }
        
    }
}

document.addEventListener('DOMContentLoaded', function (e) {
    let art_upload_form = document.getElementById('art_upload_form');
    art_upload_form.addEventListener('submit', (e) => { 
        e.preventDefault();
        validateArtTitle();
        // validateImageTile();
        // let charInput = e.target['cat'];
        // var catId = []
        // charInput.forEach(c => {
        //     if (c.checked)
        //         catId.push(c.value)
        // });
    
        let data = {
        }
        data = getData(e);
        this.catId = []
        let url = window.location.href + 'uploadArt';
        if (activeFlag) { 
            document.getElementById('btnsave').disabled = true;
            document.getElementById('btnsave').style.opacity = '0.5';
            var user_id = e.target['user_id'].value;
            axios.post(
                url, data, {
                    header:{
                        'content-type': 'multipart/form-data'
                    },
                    params: {
                        'custom': 'uploadart'
                    }
                }
            ).then(
                res => {
                    if (res.data.status) { 
                        document.getElementById('art_title').value = '';
                        document.getElementById('fileElem').value = '';
                        document.getElementById('cat').value = '';
                        document.getElementById('btnsave').disabled = false;
                        document.getElementById('btnsave').style.opacity = '2';
                        document.getElementById('gallery').innerHTML = '';
                        var successR = document.getElementById('successR');
                        successR.innerHTML = '<span>Art Uploaded Successfully</span>';
                        successR.style.display = 'block';
                        successR.style.right = 0;
                        successR.style.marginRight = '5px';
                        document.getElementById('btnsave').disabled = false;
                        setTimeout(function () {
                            var success = document.getElementById('success');
                            success.style.display = 'none';
                            window.location = document.location.origin + `/?author=${user_id}`
                        }, 1500);
                    } else {
                        var errorR = document.getElementById('errorR');
                        errorR.innerHTML = '<span>' + err.data.msg + '</span>';
                        errorR.style.right = 0;
                        errorR.style.marginRight = '5px';
                        setTimeout(function () {
                            var errorR = document.getElementById('errorR');
                            errorR.style.display = 'none';
                            activeFlag = false
                            document.getElementById('btnsave').disabled = false;
                            document.getElementById('btnsave').style.opacity = '2';
                        }, 2000);
                    }
                    
                }
            ).catch(
                err => {
                    var successR = document.getElementById('successR');
                    successR.innerHTML = '<span>Art Uploaded Successfully !</span>';
                    successR.style.right = 0;
                    successR.style.marginRight = '5px';
                    setTimeout(function () {
                        var successR = document.getElementById('errorR');
                        successR.style.display = 'none';
                        successR = false
                        document.getElementById('btnsave').disabled = false;
                        document.getElementById('btnsave').style.opacity = '2';
                        window.location = document.location.origin + `/?author=${user_id}`
                    }, 2000);
                }
            )
        }
    })
})

function getData(e) {
    let data = {};
    data = new FormData();
    data.append('art_file', e.target['fileElem'].files[0]);
    data.append('art_title', e.target['art_title'].value);
    if (e.target['post_id'].value) {
        data.append('post_id', e.target['post_id'].value);
    }
    let inputs = e.target['cat'];

    if (inputs.checked) {
        data.append('art_cat[]', inputs.value);
    }
    let catIDs = [];
    for (var i = 0; i < inputs.length; i++) {
        console.log(inputs[i], inputs[i].checked);
        if (inputs[i].checked) {

            // only record the checked inputs
            data.append('art_cat[]', inputs[i].value);
            inputs[i].checked = false;

        }
    }

    data.append('user_id', e.target['user_id'].value);
    return data;
}