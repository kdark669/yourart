
let dropArea = document.getElementById('drop-area')
    ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
    })

function preventDefaults(e) {
    e.preventDefault()
    e.stopPropagation()
}
;['dragenter', 'dragover'].forEach(eventName => {
    dropArea.addEventListener(eventName, highlight, false)
})

    ;['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false)
    })

function highlight(e) {
    dropArea.classList.add('highlight')
}

function unhighlight(e) {
    dropArea.classList.remove('highlight')
}
dropArea.addEventListener('drop', handleDrop, false)

function handleDrop(e) {
    let dt = e.dataTransfer
    let files = dt.files

    handleFiles(files)
}
function handleFiles(files) {
    var existing_image = document.getElementById('existing_image')
    if (existing_image) {
        existing_image.style.display = 'none';
    }
    files = [...files]
    for (var i = 0, len = files.length; i < len; i++) {
        if (validateImage(files[i]))
            previewFile(files[i]);
    }
}

function previewFile(file) {
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = function () {
        let img = document.createElement('img')
        img.src = reader.result
        document.getElementById('gallery').appendChild(img)
    }
}

function validateImage(files) {
    // 'image/jpeg', 
    var validTypes = ['image/png'];
    
    if (validTypes.indexOf(files.type) === -1) {
        alert("Invalid File Type");
        fileSizeBool = false
        return false;
    }

    // check the size
    var maxSizeInBytes = 10e6; // 10MB
    if (files.size > maxSizeInBytes) {
        alert("File too large");
        return false;
    }

    return true;
}