
let activeFlag = true;

function validateUsername() {
    var username = document.getElementById('user_login').value;
    if (!username) {
        var euser_login = document.getElementById('euser_login');
        euser_login.innerHTML = 'UserName is Required';
        euser_login.style.display = 'block';
        activeFlag = false;
    } else {
        var euser_login = document.getElementById('euser_login');
        euser_login.style.display = 'none';
        activeFlag = true;
    }
}

function validateFirstname(e) {
    var firstname = document.getElementById('first_name').value;
    if (!firstname) {
        var efirst_name = document.getElementById('efirst_name');
        efirst_name.innerHTML = 'FirstName is Required';
        efirst_name.style.display = 'block';
        activeFlag = false;
    } else {
        var efirst_name = document.getElementById('efirst_name');
        efirst_name.style.display = 'none';
        activeFlag = true;
    }
}

function validateLastname(e) {
    var lastName = document.getElementById('last_name').value;
    if (!lastName) {
        var elast_name = document.getElementById('elast_name');
        elast_name.innerHTML = 'LastName is Required';
        elast_name.style.display = 'block';
        activeFlag = false;
    } else {
        var elast_name = document.getElementById('elast_name');
        elast_name.style.display = 'none';
        activeFlag = true;
    }
}
function validateEmail(e) {
    var email = document.getElementById('user_email').value;
    if (!email) {
        var euser_email = document.getElementById('euser_email');
        euser_email.innerHTML = 'Email is Required';
        euser_email.style.display = 'block';
        activeFlag = true;
    } else {
        var euser_email = document.getElementById('euser_email');
        euser_email.style.display = 'none';
        activeFlag = false;
    }
}
function validatePassword(e) {
    var password = document.getElementById('user_pass').value;
    if (!password) {
        var euser_pass = document.getElementById('euser_pass');
        euser_pass.innerHTML = 'Password is Required';
        euser_pass.style.display = 'block';
        activeFlag = false;
    } else {
        var euser_pass = document.getElementById('euser_pass');
        euser_pass.style.display = 'none';
        activeFlag = true;
    }
}
document.addEventListener('DOMContentLoaded', function (e) {
    let register = document.getElementById('ya-form');
    register.addEventListener('submit', (e) => { 
        e.preventDefault();
        let data = {
            user_login: e.target['user_login'].value,
            first_name: e.target['first_name'].value,
            last_name: e.target['last_name'].value,
            user_email: e.target['user_email'].value,
            user_pass: e.target['user_pass'].value,
        };
        validateUsername();
        validateFirstname();
        validateLastname();
        validateEmail();
        validatePassword();

        let url = window.location.href + '/register';

        if (activeFlag) {
            document.getElementById('btnsave').disabled = true;
            document.getElementById('btnsave').style.opacity = '0.5';
            axios.post(
                url, data, {
                params: {
                    'custom': 'register'
                }
            }).then(
                res => {
                    if (res.data.status) {
                        e.target['user_login'].value = '';
                        e.target['first_name'].value = '';
                        e.target['last_name'].value = '';
                        e.target['user_email'].value = '';
                        e.target['user_pass'].value = '';
                        // e.target['user_repass'].value = '';
                        var success = document.getElementById('successR');
                        success.innerHTML = '<span>Welcome To BellyMonk. Please Login</span>';
                        success.style.display = 'block';
                        success.style.right = 0;
                        success.style.marginRight = '5px';
                        document.getElementById('btnsave').disabled = false;
                        setTimeout(function () {
                            var success = document.getElementById('success');
                            success.style.display = 'none';
                            window.location = document.location.origin + '/login/'
                        }, 1000);
                    }
                    else {
                        var errorR = document.getElementById('errorR');
                        errorR.innerHTML = '<span>' + res.data.msg + '</span>';
                        errorR.style.display = 'block';
                        errorR.style.right = '0';
                        errorR.style.marginRight = '5px';
                        setTimeout(function () {
                            var errorR = document.getElementById('errorR');
                            errorR.style.display = 'none';
                            activeFlag = false
                            document.getElementById('btnsave').disabled = false;
                            document.getElementById('btnsave').style.opacity = '2';
                        }, 2000);
                    }
               }
            ).catch(
                err => {
                    var errorR = document.getElementById('errorR');
                    errorR.innerHTML = '<span>' + err.data.msg + '</span>';
                    errorR.style.right = 0;
                    errorR.style.marginRight = '5px';
                    setTimeout(function () {
                        var errorR = document.getElementById('errorR');
                        errorR.style.display = 'none';
                        activeFlag = false
                        document.getElementById('btnsave').disabled = false;
                        document.getElementById('btnsave').style.opacity = '2';
                    }, 2000);
                }
            );
        }
    })
});