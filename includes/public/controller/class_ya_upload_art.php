<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

class Ya_Upload_Art
{
    public function __construct()
    {
        add_action('init', array($this, 'handle'));
    }
    public function handle()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['custom']) && $_GET['custom'] == "uploadart") {
            $existing_post_id = sanitize_text_field($_POST['post_id']);
            $upload_details= array(
                'user_id' => sanitize_text_field($_POST['user_id']),
                'art_title' => sanitize_text_field($_POST['art_title']),
                'art_file'=> $_FILES['art_file'],
                'art_cat'=> $_POST['art_cat']
            );
            if($existing_post_id){
               $this->editPost( $existing_post_id, $upload_details);
            }else{
                $this->uploadPost($upload_details);
            }
            
            exit();
        }
    }
    public function uploadPost($upload_details)
    {
        $file = $upload_details['art_file'];
        $user_id = $upload_details['user_id'];
        $art_title = $upload_details['art_title'];
        $category = $upload_details['art_cat'];
        $filename = basename($file);
        $upload_file = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
        $filename = $upload_file['file'];
        $wp_filetype = wp_check_filetype($filename, null);
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, $filename);
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
        wp_update_attachment_metadata($attach_id, $attach_data);
        $image_url = $upload_file['url'];
        if (!$upload_file['error']) {
            $wp_filetype = wp_check_filetype($filename, null);
            $post = array(
                'post_author' => $user_id,
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => $art_title,
                'post_content' => '<img class="alignnone size-medium wp-image-30" src="' . $image_url . '" alt="" width="300" height="175" />',
                'post_type' => 'art'
            );
            $post_id = wp_insert_post($post, '');
            update_post_meta($post_id, '_acceptedFlag',  0);
            set_post_thumbnail($post_id, $attach_id);
            
            global $wpdb;
            $table = $wpdb->prefix . 'itg_your_art';
            foreach ($category as $cat) {
                $data = array(
                    'post_id' => $post_id,
                    'product_id' => $cat,
                );
                $wpdb->insert($table, $data);
            }
        
            echo json_encode(array('status' => true, 'msg' => 'Uploaded Successfully'));
            die();
        }
    }

    public function editPost($existing_post_id,$upload_details){
        if(!$upload_details['art_file']){
            $existing_content = get_post($existing_post_id)->post_content;
            $post = array(
                'ID' => $existing_post_id,
                'post_author' => $upload_details['user_id'],
                'post_title' => $upload_details['art_title'],
                'post_content' => $existing_content,
                'post_type' => 'art'
            );
            $post_id = wp_update_post($post);
            update_post_meta($post_id, '_acceptedFlag',  0);
            $this->update_category_table($existing_post_id, $post_id, $upload_details);
            echo json_encode(array('status' => true, 'msg' => 'Updated Successfully'));
            die();
        }
        else{
            $existing_content = get_post($existing_post_id)->post_content;
            $file = $upload_details['art_file'];
            $the = $this->storeFile($file, $existing_post_id, $upload_details);
            $post_id = wp_update_post($the[0]);
            update_post_meta($post_id, '_acceptedFlag',  0);
            set_post_thumbnail($post_id,  $the[1]);
            $this->update_category_table($existing_post_id, $post_id, $upload_details);
            echo json_encode(array('status' => true, 'msg' => 'Updated Successfully'));
            die();

        }

    }
    public function storeFile($file, $existing_post_id, $upload_details){
        $filename = basename($file);
        $upload_file = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
        $filename = $upload_file['file'];
        $wp_filetype = wp_check_filetype($filename, null);
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, $filename);
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
        wp_update_attachment_metadata($attach_id, $attach_data);
        $image_url = $upload_file['url'];
        if (!$upload_file['error']) {
            $wp_filetype = wp_check_filetype($filename, null);
            $post = array(
                'ID' => $existing_post_id,
                'post_author' => $upload_details['user_id'],
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => $upload_details['art_title'],
                'post_content' => '<img class="alignnone size-medium wp-image-30" src="' . $image_url . '" alt="" width="300" height="175" />',
                'post_type' => 'art'
            );
        }
        $store_value = array(
            $post, $attach_id
        );
        return $store_value ;
    }
    public function update_category_table($existing_post_id, $post_id, $upload_details){
        global $wpdb;
        $table = $wpdb->prefix . 'itg_your_art';
        $category = $upload_details['art_cat'];
        if ($existing_post_id) {
            $data_delete = array(
                'post_id' => $post_id,
            );
            $wpdb->delete($table, $data_delete);

            foreach ($category as $cat) {
                $data = array(
                    'post_id' => $post_id,
                    'product_id' => $cat,
                );
                $wpdb->insert($table, $data);
            }
        }
        return ;
    }
}
return new Ya_Upload_Art();
