<?php
if (!defined('ABSPATH')) exit;
class YA_ReuseAuthor
{
    public function __construct()
    {
        add_filter('template_include', array($this,  'update_author_page'));
    }
    
    function update_author_page($template)
    {
        if (get_query_var('author_name')) {
            $author = get_user_by('slug', get_query_var('author_name'));
        }
        if (!empty($author) || isset($_GET['author'])) {
            $user_id = isset($_GET['author']) ? $_GET['author'] : $author->ID;
            if (empty($user_id)) {
                if (!is_user_logged_in()) {
                    $finds[] = ITGYA_PLUGIN_DIR . '/templates/register.php';
                    $template = locate_template(array_unique($finds));
                    if (!$template) {
                        $template =  ITGYA_PLUGIN_DIR . '/templates/register.php';
                    }
                }
            }else{
                $user_meta = get_userdata($user_id);
                $file = '';
                $user_roles = $user_meta->roles[0];
                if (empty($user_roles)) {
                        global $wp_query;
                        $wp_query->set_404();
                        status_header(404);
                        get_template_part(404);
                        exit();
                    }
                else{
                    switch ($user_roles) {
                        case "administrator":
                            echo '';
                            break;
                        case "artist":
                            if (is_author()) {
                                $file = 'profile.php'; // the name of your custom template
                                $find[] = $file;
                                $find[] = 'templates/' . $file;
                            }
                            if ($file) {
                                $template = locate_template(array_unique($find));
                                if (!$template) {
                                    // if not found in theme, will use your plugin version
                                    wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
                                    $template = ITGYA_PLUGIN_DIR . '/templates/' . $file;
                                }
                            }
                            break;
                    }
                }
            }
            return $template;
        } else {
            return $template;
        }
        
        
    }
}
return new YA_ReuseAuthor();
