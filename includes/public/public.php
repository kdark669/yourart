<?php
require_once(ITGYA_PLUGIN_DIR . '/includes/public/controller/class_ya_reuse_author.php');
if (!is_admin()) {
    require_once(ITGYA_PLUGIN_DIR . '/includes/public/controller/class_ya_register.php');
    require_once(ITGYA_PLUGIN_DIR . '/includes/public/controller/class_ya_upload_art.php');
}




add_action('wp_head', 'custom_redirect_to_userdetail');
function custom_redirect_to_userdetail()
{
    $page = get_page_by_title('Sell Your Art');
    $url = $page->guid;
    //check user is login or not 
    if (!is_user_logged_in()){
?>
   
    <a href="<?php echo $url ?>">
        Sell Your Art
    </a>
<?php
}}
?>