<?php
if (!defined('ABSPATH')) exit;
class YA_Custom_Page
{
    function ya_create_custom_page()
    {
        global $user_ID;
        //upload action page
        $upload_action_page_title = 'Upload Art';
        $upload_action_page = array(
            'post_title' => $upload_action_page_title,
            'post_status' => 'publish', // Automatically publish the post.
            'post_author' => $user_ID,
            'post_type' => 'page',
            'post_parent' => 0,
            'post_name' => 'uploadart',
        );
        if (get_page_by_title($upload_action_page_title, 'page', 'page') == NULL) {
            $post_id = wp_insert_post($upload_action_page);
            if (!$post_id)
                wp_die('Error creating template page');
            else {
                update_post_meta($post_id, '_wp_page_template', 'uploadart.php');
            }
        }
       
        //sell ur page 
        $page_sell_title = 'Sell Your Art';
        $sell_ur_art_page = array(
            'post_title' => $page_sell_title,
            'post_status' => 'publish', // Automatically publish the post.
            'post_author' => $user_ID,
            'post_category' => array(1, 3), // Add it two categories.
            'post_type' => 'page',
            'post_parent'=>0,
            'post_name' => 'sellyourart', // defaults to "post". Can be set to CPTs.
        );
        if (get_page_by_title($page_sell_title, 'page', 'page') == NULL) {
            $post_id = wp_insert_post($sell_ur_art_page);
            if (!$post_id)
                wp_die('Error creating template page');
            else{
                update_post_meta($post_id, '_wp_page_template', 'sellyourart.php');
            }
        }

    }

    function ya_catch_plugin_template($template)
    {
        if (is_page_template('sellyourart.php')) {
            if (!is_user_logged_in()) {
                $template = ITGYA_PLUGIN_DIR . '/templates/sellyourart.php';
                return $template;
            } else {
                wp_redirect(site_url());
            }
        }
        if (is_page_template('uploadart.php')) {
            if (is_user_logged_in()) {
                $template = ITGYA_PLUGIN_DIR . '/templates/uploadart.php';
                return $template;
            } else {
                $template = ITGYA_PLUGIN_DIR . '/templates/register.php';
                return $template;
            }
        }
        return $template;
    }
}
return new YA_Custom_Page();
