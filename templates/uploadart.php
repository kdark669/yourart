<?php
wp_enqueue_style('upload-art', ITGYA_PLUGIN_URL . '/includes/public/assets/css/uploadart.css');
wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
wp_enqueue_script('upload-art-form', ITGYA_PLUGIN_URL . '/includes/public/assets/js/uploadArt.js', array(), false, true);
wp_enqueue_script('upload-art', ITGYA_PLUGIN_URL . '/includes/public/assets/js/draganddrop.js', array(), false, true);
$url = site_url();
get_header();
$post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
$user_id = get_current_user_id();
$args = array('post_type' => 'product', 'product_cat' => 'Uncategorized', 'orderby' => 'rand');
$category = new WP_Query($args);
$products = $category->posts;
$product_ids = array_map(function ($data) {
    return $data->ID;
}, $products);

//fetch post indo by post_id to edit using same form


?>
<section class="profile-view">
    <div class="updated notice success" id="successR">
    </div>
    <div class="errorR notice" id="errorR">
    </div>
    <div class="upload-art">
        <div class="upload-art-container">
            <div>
                <div class="upload-art-caption">
                    Upload an art
                </div>
                <div class="upload-art-form">
                    <form action="" class="upload-form" id="art_upload_form" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id ?>">

                        <div class="form-group">
                            <?php
                            if ($post_id) {
                                $title = get_post($post_id)->post_title;
                            ?>
                                <input type="text" name="art_title" id="art_title" placeholder="Title" onblur="validateArtTitle()" value="<?php echo $title ?>">
                            <?php
                            } else {
                            ?>
                                <input type="text" name="art_title" id="art_title" placeholder="Title" onblur="validateArtTitle()" ">
                        <?php
                            }
                        ?>
                        <small class=" error " id=" eart_title"></small>
                        </div>
                        <div class="form-group">
                            <div id="drop-area">
                                <?php
                                if ($post_id) {
                                    $images = get_post($post_id)->post_content;

                                ?>
                                    <p>images must be in png </p>
                                    <p>Drag Here To Upload Your File</p>
                                    <input type="file" id="fileElem" name="artFile" accept="image/*" onchange="handleFiles(this.files)" onblur="validateImage()">
                                    <label class="choose_file" for="fileElem">
                                        <p>or Choose A file</p>
                                    </label>
                                    <div class="existing_image" id="existing_image"><?php echo  $images ?></div>
                                    <div id="gallery">
                                    </div>
                                    <small class="error " id="eart_image"></small>
                                <?php
                                } else {
                                ?>
                                    <p>images must be in png </p>
                                    <p>Drag Here To Upload Your File</p>
                                    <label class="choose_file" for="fileElem">
                                        <p style="text-align: center;">or Click Here</p>
                                    </label>
                                    <input type="file" id="fileElem" name="artFile" accept="image/*" onchange="handleFiles(this.files)" onblur="validateImage()">
                                    <div id="gallery">
                                    </div>
                                    <small class="error " id="eart_image"></small>
                                <?php
                                }
                                ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="product-category">
                                <div class="product-category-list">
                                    <p>category list </p>
                                    <div class="category-product-container">
                                        <?php
                                        if ($post_id) {
                                            global $wpdb;
                                            $postTable = $wpdb->prefix . 'itg_your_art';
                                            $productCategories = $wpdb->get_results(" SELECT product_id FROM $postTable where post_id = $post_id");
                                            if ($productCategories) {
                                                foreach ($productCategories as $pc) {
                                                    foreach ($products as $p) {
                                                        $img = get_the_post_thumbnail_url($p->ID);
                                                        if ($pc->product_id == $p->ID) {
                                                            echo '<div class="product-container ">
                                                            <img src="' . $img . '" alt="product-images"/>
                                                            <input type="checkbox" checked class="checkbox" id="cat' . $p->ID . '" name="cat" value="' . $p->ID . '"/>
                                                        </div>
                                                        ';
                                                        } else {
                                                            echo '<div class="product-container ">
                                                            <img src="' . $img . '" alt="product-images"/>
                                                            <input type="checkbox"  class="checkbox" id="cat' . $p->ID . '" name="cat" value="' . $p->ID . '"/>
                                                        </div>
                                                        ';
                                                        }
                                                    }
                                                }
                                            } else {
                                                foreach ($products as $p) {
                                                    $img = get_the_post_thumbnail_url($p->ID);
                                                    echo '<div class="product-container ">
                                                <img src="' . $img . '" alt="product-images"/>
                                                <input type="checkbox" class="checkbox" id="cat' . $p->ID . '" name="cat" value="' . $p->ID . '"/>
                                            </div>
                                            ';
                                                }
                                            }
                                        } else {
                                            foreach ($products as $p) {
                                                $img = get_the_post_thumbnail_url($p->ID);
                                                echo '<div class="product-container ">
                                                <img src="' . $img . '" alt="product-images"/>
                                                <input type="checkbox" class="checkbox" id="cat' . $p->ID . '" name="cat" value="' . $p->ID . '"/>
                                            </div>
                                            ';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="upload-button" id="btnsave">
                            Upload
                        </button>
                    </form>
                </div>
            </div>

        </div>

    </div>
</section>
<?php
get_footer();
