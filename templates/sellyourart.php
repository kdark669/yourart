<?php
/* Template Name: sellyourart */
wp_enqueue_style('sellurart', ITGYA_PLUGIN_URL . '/includes/public/assets/css/sellurart.css');
$url = site_url();
get_header();
?>
<section class="sellurart">
    <div class="sell-banner">
        <div class="sell-banner-img">
            <img src="<?php echo ITGYA_PLUGIN_URL . '/includes/public/assets/img/Frame.png' ?>" alt="bannerimages" class="sellurart-img">
            <div class="sell">
                <div class="sell-form-title">
                    EASIEST WAY TO SELL YOUR ART ONLINE
                </div>
                <form action="" class="sell-form" method="POST" id="sell-form">
                    <input type="text" value="" name="sell_email" id="sell_email" placeholder="Enter Your Email Address " onblur="validateEmail()">
                    <div></div>
                    <button type="submit" class="join-us-button">Join Us</button>
                </form>
                <small class="invalid-feedback error form-text text-dark font-weight-bold" id="esell_email"></small>

                <script type="text/javascript">
                    let activeFlag = true;

                    function validateEmail(e) {
                        var email = document.getElementById('sell_email').value;
                        if (!email) {
                            var euser_email = document.getElementById('esell_email');
                            euser_email.innerHTML = 'Email is Required';
                            euser_email.style.display = 'block';
                            activeFlag = false;
                        } else {
                            var euser_email = document.getElementById('esell_email');
                            euser_email.style.display = 'none';
                            activeFlag = true;
                        }
                    }
                    document.addEventListener('DOMContentLoaded', function(e) {
                        let sellForm = document.getElementById('sell-form');
                        sellForm.addEventListener('submit', (e) => {
                            e.preventDefault();
                            validateEmail();
                            let site_url = document.location.origin
                            let sell_email = e.target['sell_email'].value
                            if (activeFlag) {
                                document.location.href = `${site_url}/?email=${sell_email}&author`;
                            }
                        })
                    })
                </script>
            </div>
        </div>

    </div>

    <div class="sell-main-section">
        <div class="how-its-works">
            How it work
        </div>
        <div></div>
        <div class="steps">
            <div class="steps-images">
                <img src="<?php echo ITGYA_PLUGIN_URL . '/includes/public/assets/img/steps.png' ?>" alt="steps">
            </div>
        </div>

        <div class="final-thoughts">
            <a href="<?php echo site_url() . '/?author' ?>">
                <img src="<?php echo ITGYA_PLUGIN_URL . '/includes/public/assets/img/last.png' ?>" alt="steps">
            </a>
        </div>
    </div>
</section>


<?php
get_footer();