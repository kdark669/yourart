<?php
wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
wp_enqueue_script('ya-form', ITGYA_PLUGIN_URL . '/includes/public/assets/js/register.js', array(), false, true);
wp_enqueue_style('register-artist', ITGYA_PLUGIN_URL . '/includes/public/assets/css/register.css');
get_header();

$email = isset($_GET['email']) ? $_GET['email'] : null;
$categories = get_categories(array(
    'taxonomy' => 'product_cat',
    'orderby' => 'name',
    'parent' => 0,
    'hide_empty' => 0,
));

?>

<main class="register-product-section">
    <div class="updated notice success" id="successR">
    </div>
    <div class="errorR notice" id="errorR">
    </div>
    <div class="register-artist">
        <div class="container">
            <div class="register-container">
                <div class="register-banner">
                    <img src="<?php echo ITGYA_PLUGIN_URL . '/includes/public/assets/img/Beapart.png' ?>" alt="images">
                </div>
                <div class="register-form">
                    <div class="form-title">Open Your Shop</div>
                    <div class="login-container ">
                        <p>Already Have Account?<a href="<?php echo site_url() . '/my-account' ?>" style="color: #3b99fc;">Login here</a></p>
                    </div>
                    <form id="ya-form" method="POST">
                        <div class="form-group">
                            <input type="text" name="user_login" id="user_login" class=" form-control form-control-lg border border-dark font-weight-bold" aria-describedby="usernameHelp" placeholder="Username" required="required" onblur="validateUsername()">
                            <small class="invalid-feedback error form-text text-dark font-weight-bold" id="euser_login"></small>
                        </div>
                        <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class=" form-control form-control-lg border border-dark font-weight-bold" aria-describedby="usernameHelp" placeholder="First Name" required="required" onblur="validateFirstname(event)">
                            <small class="invalid-feedback error form-text text-dark font-weight-bold" id="efirst_name"></small>
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" id="last_name" class=" form-control form-control-lg border border-dark font-weight-bold" aria-describedby="usernameHelp" placeholder="Last Name" required="required" onblur="validateLastname(event)">
                            <small class=" invalid-feedback error form-text text-dark font-weight-bold" id="elast_name"></small>
                        </div>
                        <div class="form-group">
                            <input type="text" name="user_email" id="user_email" class="form-control form-control-lg border border-dark font-weight-bold" aria-describedby="usernameHelp" placeholder="Your Email" required="required" onblur="validateEmail(event)" value="<?php echo $email ?>">
                            <small class="invalid-feedback error form-text text-dark font-weight-bold" id="euser_email"></small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="user_pass" id="user_pass" class="form-control form-control-lg border border-dark font-weight-bold" aria-describedby="usernameHelp" placeholder="Your Password" required="required" onblur="validatePassword(event)">
                            <small class="invalid-feedback error form-text text-dark font-weight-bold" id="euser_pass"></small>
                        </div>
                        <div class="errorPassword error" id="errorPassword">
                        </div>

                        <div class="form-group input-field input-submit">
                            <button type="submit" id="btnsave" class="buttonsave border border-dark" name="btnsave"><?php echo __('Submit', 'YourArts') ?> </button>
                            <div class="ya_ajax_loader" style="display: none;"><img src="<?php echo ITGYA_PLUGIN_URL; ?>/includes/public/assets/img/ajax_loader.gif"></div>
                        </div>
                        <div class="ya-message-container"></div>
                    </form>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <hr>
    <div class="product">
        <div class="product-collection">
            <h3>Our Products</h3>
            <div class="category-list">
                <?php
                foreach ($categories as $category) {
                    if ($category->name != "Uncategorized") {
                ?>
                        <div class="category-title">
                            <h3><?php echo $category->name; ?></h3>
                        </div>
                        <div class="category-list-item">
                            <?php
                            $args = array(
                                'type' => 'post',
                                'child_of' => $category->term_id,
                                'orderby' => 'name',
                                'order' => 'ASC',
                                'hide_empty' => FALSE,
                                'hierarchical' => 1,
                                'taxonomy' => 'product_cat',
                            );
                            $childCategories = get_categories($args);
                            foreach ($childCategories as $childCategory) : ?>
                                <?php
                                $thumbnail_id = get_term_meta($childCategory->term_id, 'thumbnail_id', true);
                                $image = wp_get_attachment_url($thumbnail_id);
                                $imageLink = get_category_link($childCategory->term_id);
                                ?>
                                <?php if (!empty($image)) : ?>
                                    <div class="category-description">
                                        <img class="category-image" src="<?php echo $image ?>" alt="category">
                                        <div class="child-name"><?php echo $childCategory->name ?></div>
                                    </div>
                                <?php endif; ?>
                            <?php

                            endforeach;
                            ?>

                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</main>
<?php
get_footer();
exit();
?>