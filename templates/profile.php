<?php
wp_enqueue_style('profile', ITGYA_PLUGIN_URL . '/includes/public/assets/css/profile.css');
wp_enqueue_script('upload-art', ITGYA_PLUGIN_URL . '/includes/public/assets/js/changeProfile.js', array(), false, true);
get_header();
$postTable = $wpdb->prefix . 'posts';
$authorTable = $wpdb->prefix . 'itg_art_profile';

$current_user_id = get_current_user_id();
$author_id =  isset($_GET['author']) ? $_GET['author'] : get_query_var('author');
?>
<div class="container">
    <main class="user-profile">
        <?php
        show_profile_card($current_user_id, $author_id);
        if (is_user_logged_in() && ($current_user_id == $author_id_url)) {
            displayCollectionOrArtUpload($author_post, $current_user_id, $author_id, true);
        } else {
            displayCollectionOrArtUpload($author_post, $current_user_id, $author_id, false);
        }
        ?>
    </main>
</div>
<?php
function show_profile_card($current_user_id, $author_id)
{
    $page = get_page_by_title('Upload Art');
    $pageUrl = $page->guid;
    $user_data = get_userdata($author_id);
?>
    <section class="profile">
        <div class="profile-card">
            <div class="banner">
                <img src="<?php echo ITGYA_PLUGIN_URL . '/includes/public/assets/img/banner.png' ?>" class="banner-img">
            </div>
            <div class="profile-info">
                <div class="profile-pic">
                    <img src="https://img.icons8.com/plasticine/2x/user.png">
                </div>
                <div class="artist-info">
                    <p class="artist-name"><?php echo $user_data->display_name ?></p>
                    <p class="artist-name"><?php echo $user_data->roles[0] ?></p>
                    <p class="artist-bio"><?php echo $user_data->user_email ?></p>
                </div>
                <?php if ($current_user_id == $author_id) { ?>
                    <a href="<?php echo $pageUrl ?>" class="profile-arts flex-centered" data-popup-trigger="one">
                        <div class="art-upload-btn">
                            <button type="submit" class="btn-md btn-dark font-weight-bold btn-block shadow-none">
                                UPLOAD YOUR ART
                            </button>
                        </div>
                    </a>
                <?php } ?>
            </div>

        </div>
    </section>
<?php } ?>
<hr>

<?php function displayCollectionOrArtUpload($current_user_id, $author_id)
{
    $args_authors_posts = array(
        'post_type'      => 'art',
        'author'         => $author_id,
        'post_status' => 'any',
        'posts_per_page' => -1
    );
    $author_post = get_posts($args_authors_posts);

    $args_category = array('post_type' => 'product', 'product_cat' => 'Uncategorized', 'orderby' => 'rand');
    $category = new WP_Query($args_category);
    $products = $category->posts;
    $product_ids = array_map(function ($data) {
        return $data->ID;
    }, $products);
?>
    <?php do_action('form_message'); ?>
    <section class="art-collection">
        <div class="container">
            <div class="heading-collection">
                <h3>Collections</h3>
            </div>
            <div class="collection-list">
                <?php
                if (empty($author_post)) {
                ?>
                    <p>Collection is Empty</p>
                <?php
                }
                $page = get_page_by_title('Upload Art');
                $pageUrl = $page->guid;
                // print_r($pageUrl);
                // die();
                $url = site_url();
                foreach ($author_post as $a) {
                    $a_meta = get_post_meta($a->ID);
                    $acceptedFlag = $a_meta['_acceptedFlag'][0];
                ?>
                    <div class="collection-images">
                        <?php if ($acceptedFlag) { ?>
                            <a href="<?php echo $url ?>/?s=<?php echo $a->post_title ?>&post_type=product">
                                <?php echo $a->post_content ?>
                                <div class="child-name"><?php echo $a->post_title ?></div>
                            </a>
                        <?php } else {
                            $edit_url = "$url/uploadArt/?post_id=$a->ID";
                        ?>
                            <?php echo $a->post_content ?>
                            <div class="child-name"><?php echo $a->post_title ?></div>
                            <a class="art-upload-btn" href="<?php echo $edit_url; ?>">
                                <button>
                                    Edit
                                </button>
                            </a>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                }
                ?>
            </div>

        </div>
    </section>
<?php } ?>

<?php
get_footer();
?>